stages:
  - "Ubuntu rolling"
  - "Ubuntu 24.10"
  - "Ubuntu 24.04"
  - "Ubuntu 23.10"
  - "Ubuntu 22.04"
  - "Ubuntu 20.04"
  - "Ubuntu 18.04"
  - "Debian trixie"
  - "Debian 12"
  - "Debian 11"
  - "Debian 10"

.packaging:
  base:
    script:
      # Needed by Ubuntu 20.04, 18.04
      - export DEBIAN_FRONTEND="noninteractive"

      - apt update -y
      - apt upgrade -y

      - apt install build-essential -y
  build:
    script:
      - apt install git -y
      - git clone --branch "v${VERSION}" https://github.com/digitaltrails/ddcutil-service.git

      - ( cd ddcutil-service && make )
  package:
    script:
      - mkdir -p "${FULL_NAME}/usr/bin"
      - cp ddcutil-service/ddcutil-service "${FULL_NAME}/usr/bin/ddcutil-service"

      - mkdir -p "${FULL_NAME}/usr/share/dbus-1/services"
      - cp ddcutil-service/com.ddcutil.DdcutilService.service "${FULL_NAME}/usr/share/dbus-1/services/com.ddcutil.DdcutilService.service"

      - mkdir -p "${FULL_NAME}/DEBIAN"
      - cp template/DEBIAN/control "${FULL_NAME}/DEBIAN/control"
      - sed -i "s/libddcutil-dev,/libddcutil-dev (= $(dpkg -s libddcutil-dev | grep '^Version:' | awk '{print $2}')),/" "${FULL_NAME}/DEBIAN/control"
      - "echo \"Version: ${VERSION}\" >> \"${FULL_NAME}/DEBIAN/control\""
      - "echo \"Installed-Size: $(du -c --apparent-size ddcutil-service/{ddcutil-service,com.ddcutil.DdcutilService.service} | tail -n 1 | awk '{print $1}')\" >> \"${FULL_NAME}/DEBIAN/control\""
      - cat "${FULL_NAME}/DEBIAN/control"

      - dpkg -b "${FULL_NAME}"
  test:
    script:
      - apt install "./${FULL_NAME}.deb" -y

      - "[ -e /usr/bin/ddcutil-service ]"
      - "[ -e /usr/share/dbus-1/services/com.ddcutil.DdcutilService.service ]"
  publish:
    script:
      - apt install curl -y
      - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${FULL_NAME}.deb "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_JOB_STAGE/ /-}/${VERSION}/${FULL_NAME}.deb"'

.distro:
  variables:
    ARCH: amd64
    FULL_NAME: ddcutil-service_${VERSION}+${CODE_NAME}-${VARIANT}_${ARCH}
    VARIANT: distro
  script:
    - !reference [ .packaging, base, script ]
    - apt install libglib2.0-dev libddcutil-dev -y
    - !reference [ .packaging, build, script ]
    - !reference [ .packaging, package, script ]
    - !reference [ .packaging, test, script ]
    - !reference [ .packaging, publish, script ]
  needs: []

.latest_ppa:
  variables:
    ARCH: amd64
    FULL_NAME: ddcutil-service_${VERSION}+${CODE_NAME}-${VARIANT}_${ARCH}
    VARIANT: ppa-latest
  script:
    - !reference [ .packaging, base, script ]
    - apt install software-properties-common -y
    - add-apt-repository ppa:rockowitz/ddcutil -y
    - apt update -y
    - apt install libglib2.0-dev libddcutil-dev -y
    - !reference [ .packaging, build, script ]
    - !reference [ .packaging, package, script ]
    - !reference [ .packaging, test, script ]
    - !reference [ .packaging, publish, script ]
  needs: []

.latest_obs:
  variables:
    ARCH: amd64
    FULL_NAME: ddcutil-service_${VERSION}+${CODE_NAME}-${VARIANT}_${ARCH}
    VARIANT: obs-latest
  script:
    - !reference [ .packaging, base, script ]
    - apt install curl gpg -y
    - echo "deb http://download.opensuse.org/repositories/home:/rockowitz/${SUSE_NAME}/ /" | tee /etc/apt/sources.list.d/home:rockowitz.list
    - curl -fsSL "https://download.opensuse.org/repositories/home:rockowitz/${SUSE_NAME}/Release.key" | gpg --dearmor | tee /etc/apt/trusted.gpg.d/home_rockowitz.gpg > /dev/null
    - apt update -y
    - apt install libglib2.0-dev libddcutil-dev -y
    - !reference [ .packaging, build, script ]
    - !reference [ .packaging, package, script ]
    - !reference [ .packaging, test, script ]
    - !reference [ .packaging, publish, script ]
  needs: []


"1.0.12+oracular-ppa-latest":
  image: ubuntu:24.10
  stage: "Ubuntu 24.10"
  extends: .latest_ppa
  variables:
    VERSION: 1.0.12
    CODE_NAME: oracular

"1.0.12+noble-ppa-latest":
  image: ubuntu:24.04
  stage: "Ubuntu 24.04"
  extends: .latest_ppa
  variables:
    VERSION: 1.0.12
    CODE_NAME: noble

"1.0.12+jammy-ppa-latest":
  image: ubuntu:22.04
  stage: "Ubuntu 22.04"
  extends: .latest_ppa
  variables:
    VERSION: 1.0.12
    CODE_NAME: jammy

"1.0.12+focal-ppa-latest":
  image: ubuntu:20.04
  stage: "Ubuntu 20.04"
  extends: .latest_ppa
  variables:
    VERSION: 1.0.12
    CODE_NAME: focal


"1.0.12+noble-obs-latest":
  image: ubuntu:24.04
  stage: "Ubuntu 24.04"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: noble
    SUSE_NAME: xUbuntu_24.04

"1.0.12+jammy-obs-latest":
  image: ubuntu:22.04
  stage: "Ubuntu 22.04"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: jammy
    SUSE_NAME: xUbuntu_22.04

"1.0.12+focal-obs-latest":
  image: ubuntu:20.04
  stage: "Ubuntu 20.04"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: focal
    SUSE_NAME: xUbuntu_20.04

"1.0.12+bionic-obs-latest":
  image: ubuntu:18.04
  stage: "Ubuntu 18.04"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: bionic
    SUSE_NAME: xUbuntu_18.04
  when: manual

"1.0.12+trixie-obs-latest":
  image: debian:trixie
  stage: "Debian trixie"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: trixie
    SUSE_NAME: Debian_Testing

"1.0.12+bookworm-obs-latest":
  image: debian:12
  stage: "Debian 12"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: bookworm
    SUSE_NAME: Debian_12

"1.0.12+bullseye-obs-latest":
  image: debian:11
  stage: "Debian 11"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: bullseye
    SUSE_NAME: Debian_11

"1.0.12+buster-obs-latest":
  image: debian:10
  stage: "Debian 10"
  extends: .latest_obs
  variables:
    VERSION: 1.0.12
    CODE_NAME: buster
    SUSE_NAME: Debian_10
  when: manual


"1.0.12+rolling-distro":
  image: ubuntu:rolling
  stage: "Ubuntu rolling"
  extends: .distro
  variables:
    VERSION: 1.0.12
    CODE_NAME: rolling

"1.0.12+oracular-distro":
  image: ubuntu:24.10
  stage: "Ubuntu 24.10"
  extends: .distro
  variables:
    VERSION: 1.0.12
    CODE_NAME: oracular

"1.0.12+noble-distro":
  image: ubuntu:24.04
  stage: "Ubuntu 24.04"
  extends: .distro
  variables:
    VERSION: 1.0.12
    CODE_NAME: noble

"1.0.12+trixie-distro":
  image: debian:trixie
  stage: "Debian trixie"
  extends: .distro
  variables:
    VERSION: 1.0.12
    CODE_NAME: trixie

"1.0.12+bookworm-distro":
  image: debian:12
  stage: "Debian 12"
  extends: .distro
  variables:
    VERSION: 1.0.12
    CODE_NAME: bookworm
