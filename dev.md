# Development

Package installs two files

```
/usr
  /bin/ddcutil-service
  /share/dbus-1/services
```

Package creation involves:
- Building `ddcutil-service` with `make`.
- Placing output files in a desired directory structure.
- Describing the package with `DEBIAN/control` meta file.
- Calling `dpkg -b` to build a `.deb` package.

The complete procedure can be found in [.gitlab-ci.yml](.gitlab-ci.yml).

`ddcutil` package is listed in the dependencies to load `i2c-dev` module and provide Udev rules.
