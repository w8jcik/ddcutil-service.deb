# _ddcutil-service_ Ubuntu and Debian packages

This repository is an external effort to package [_ddcutil-service_](https://github.com/digitaltrails/ddcutil-service).

## Usage

Binary packages can be downloaded from the [package registry](https://gitlab.com/w8jcik/ddcutil-service.deb/-/packages).

There are three variants of packages called _distro_, _PPA latest_ and _OBS latest_.

### _Distro_

_Distro_ packages are built against `libddcutil` libraries offered by the distribution.

|                |  `ddcutil`  | _ddcutil-service_ package name |
|---------------:|:-----------:|:--|
| Ubuntu rolling |    1.4.1    | [`ddcutil-service_1.0.12+rolling-distro_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-rolling/1.0.12/ddcutil-service_1.0.12+rolling-distro_amd64.deb) |
|   Ubuntu 24.10 |    1.4.1    | [`ddcutil-service_1.0.12+oracular-distro_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-24.10/1.0.12/ddcutil-service_1.0.12+oracular-distro_amd64.deb) |
|   Ubuntu 24.04 |    1.4.1    | [`ddcutil-service_1.0.12+noble-distro_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-24.04/1.0.12/ddcutil-service_1.0.12+noble-distro_amd64.deb) |
|   Ubuntu 22.04 |    1.2.2    | – |
|   Ubuntu 20.04 |      –      | – |
|   Ubuntu 18.04 |      –      | – |
|  Debian trixie |    1.4.1    | [`ddcutil-service_1.0.12+trixie-distro_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Debian-trixie/1.0.12/ddcutil-service_1.0.12+trixie-distro_amd64.deb) |
|      Debian 12 |    1.4.1    | [`ddcutil-service_1.0.12+bookworm-distro_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Debian-12/1.0.12/ddcutil-service_1.0.12+bookworm-distro_amd64.deb) |
|      Debian 11 |    0.9.9    | – |
|      Debian 10 |      –      | – |

_Distro_ packages can be installed from the terminal right away, for example on Ubuntu 24.04

```bash
sudo apt install ./ddcutil-service_1.0.12+noble-distro_amd64.deb
```

### _PPA latest_

These packages are using the latest versions of `libddcutil` libraries published by `ddcutil`'s author through Ubuntu _Personal Package Archives_ (PPA).

|              |  `ddcutil`  | _ddcutil-service_ package name |
|-------------:|:-----------:|:--|
| Ubuntu 24.10 |    2.2.0    | [`ddcutil-service_1.0.12+oracular-ppa-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-24.10/1.0.12/ddcutil-service_1.0.12+oracular-ppa-latest_amd64.deb) |
| Ubuntu 24.04 |    2.2.0    | [`ddcutil-service_1.0.12+noble-ppa-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-24.04/1.0.12/ddcutil-service_1.0.12+noble-ppa-latest_amd64.deb) |
| Ubuntu 22.04 |    2.2.0    | [`ddcutil-service_1.0.12+jammy-ppa-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-22.04/1.0.12/ddcutil-service_1.0.12+jammy-ppa-latest_amd64.deb) |
| Ubuntu 20.04 |    2.2.0    | [`ddcutil-service_1.0.12+focal-ppa-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-20.04/1.0.12/ddcutil-service_1.0.12+focal-ppa-latest_amd64.deb) |

As a consequence one needs to install `ddcutil` and `libddcutil-dev` first, from the [same source](https://launchpad.net/~rockowitz/+archive/ubuntu/ddcutil).  

Example installation on Ubuntu 24.04

```bash
sudo apt install software-properties-common
sudo add-apt-repository ppa:rockowitz/ddcutil
sudo apt update
sudo apt install ddcutil libddcutil-dev
```

```bash
sudo apt install ./ddcutil-service_1.0.12+noble-ppa-latest_amd64.deb
```

### _OBS latest_

These packages are using the latest version of `libddcutil` libraries published by `ddcutil`'s author through _openSUSE Build System_ (OBS).

|                  |  `ddcutil`  | _ddcutil-service_ package name |
|-----------------:|:-----------:|:--|
|  Ubuntu rolling  |      –      | – |
|   Ubuntu 24.04   |    2.2.0    | [`ddcutil-service_1.0.12+noble-obs-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-24.04/1.0.12/ddcutil-service_1.0.12+noble-obs-latest_amd64.deb) |
|   Ubuntu 22.04   |    2.2.0    | [`ddcutil-service_1.0.12+jammy-obs-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-22.04/1.0.12/ddcutil-service_1.0.12+jammy-obs-latest_amd64.deb) |
|   Ubuntu 20.04   |    2.1.4    | [`ddcutil-service_1.0.12+focal-obs-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Ubuntu-20.04/1.0.12/ddcutil-service_1.0.12+focal-obs-latest_amd64.deb) |
|   Ubuntu 18.04   |      ?      | – / upstream certificate issue |
|   Debian trixie  |    2.2.0    | [`ddcutil-service_1.0.12+trixie-obs-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Debian-trixie/1.0.12/ddcutil-service_1.0.12+trixie-obs-latest_amd64.deb) |
|     Debian 12    |    2.2.0    | [`ddcutil-service_1.0.12+bookworm-obs-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Debian-12/1.0.12/ddcutil-service_1.0.12+bookworm-obs-latest_amd64.deb) |
|     Debian 11    |    2.2.0    | [`ddcutil-service_1.0.12+bullseye-obs-latest_amd64.deb`](https://gitlab.com/api/v4/projects/w8jcik%2fddcutil-service.deb/packages/generic/Debian-11/1.0.12/ddcutil-service_1.0.12+bullseye-obs-latest_amd64.deb) |
|     Debian 10    |      ?      | – / upstream certificate issue |

As a consequence one needs to install `ddcutil` and `libddcutil-dev` first, from the [same source](https://software.opensuse.org/download.html?project=home%3Arockowitz&package=ddcutil).  

Example installation on Ubuntu 24.04 (following [_OBS_ instructions](https://software.opensuse.org/download.html?project=home%3Arockowitz&package=ddcutil))

```bash
echo 'deb http://download.opensuse.org/repositories/home:/rockowitz/xUbuntu_24.04/ /' | sudo tee /etc/apt/sources.list.d/home:rockowitz.list
curl -fsSL https://download.opensuse.org/repositories/home:rockowitz/xUbuntu_24.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_rockowitz.gpg > /dev/null
sudo apt update
sudo apt install ddcutil libddcutil-dev
```

```bash
sudo apt install ./ddcutil-service_1.0.12+noble-obs-latest_amd64.deb
```
